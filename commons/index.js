"use strict";
const path = require('path');
const fs = require('fs');

const dataPath = path.resolve(__dirname, './images'); 
const appdataPath = path.resolve(__dirname, './weights');

exports.getDataPath = () => dataPath;
exports.getAppdataPath = () => appdataPath;

exports.ensureAppdataDirExists = () => {
  if (!fs.existsSync(appdataPath)) {
    fs.mkdirSync(appdataPath);
  }
}

exports.readImFromBase64 = function(image, FileName, type, n=null){
	var ImageFilePath = './images/faces/'+FileName+ '_' + n + type;
	var buff = Buffer.from(image.replace(/^data:image\/(png|gif|jpeg);base64,/,''),'base64');
	fs.writeFileSync(ImageFilePath, buff);
	console.log(`${FileName}_${n}${type} done`);
	return ImageFilePath;
}

exports.__esModule = true;
var env_1 = require("./env");
exports.canvas = env_1.canvas;
var faceDetection_1 = require("./faceDetection");
exports.faceDetectionNet = faceDetection_1.faceDetectionNet;
exports.faceDetectionOptions = faceDetection_1.faceDetectionOptions;