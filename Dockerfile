FROM ubuntu
VOLUME ["/data/db"]
WORKDIR /facerecognition
COPY . /facerecognition
RUN apt-get update && \
apt-get install gnupg2 -y
RUN apt-get install -y curl
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash -
RUN apt-get install -y nodejs
RUN apt-get install -y aptitude
RUN aptitude install -y npm
RUN apt-get install -y build-essential
RUN apt-get install -y mongodb
RUN apt-get install -y supervisor
RUN npm install
COPY ./supervisord.conf /etc/supervisor
CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/supervisord.conf"]
